import { IHandler } from "@aicacia/router";
import { parse } from "url";
import { router } from "../lib";
import { confirmCurrentUserEmail } from "../stores/currentUser";
import { IRouterContext } from "../stores/lib/router";

export const confirmUserEmail: IHandler = (context: IRouterContext) =>
  confirmCurrentUserEmail(context.params.confirmation_token)
    .then(() => {
      context.redirectUrl = parse(router.paths.Index(), true);
      return context;
    })
    .catch(() => {
      context.redirectUrl = parse(router.paths.Error404(), true);
      return context;
    });
