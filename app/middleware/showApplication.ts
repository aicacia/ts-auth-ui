import { IHandler } from "@aicacia/router";
import { parse } from "url";
import { router } from "../lib";
import { showApplication as show } from "../stores/applications";
import { IRouterContext } from "../stores/lib/router";

export const showApplication: IHandler = (context: IRouterContext) =>
  show(+context.params.application_id)
    .then(() => context)
    .catch(() => {
      context.redirectUrl = parse(router.paths.Error404(), true);
      return context;
    });
