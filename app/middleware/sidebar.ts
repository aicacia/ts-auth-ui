import { IHandler } from "@aicacia/router";
import { close, open } from "../stores/sidebar";

export const sidebar = (value: boolean): IHandler => () => {
  if (value) {
    open();
  } else {
    close();
  }
};
