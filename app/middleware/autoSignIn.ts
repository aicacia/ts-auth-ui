import { IHandler } from "@aicacia/router";
import { autoCurrentUserSignIn } from "../stores/currentUser";
import { IRouterContext } from "../stores/lib/router";

export const autoSignIn: IHandler = (context: IRouterContext) =>
  autoCurrentUserSignIn()
    .then(() => context)
    .catch(() => context);
