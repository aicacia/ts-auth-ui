import { IHandler } from "@aicacia/router";
import { parse } from "url";
import { router } from "../lib";
import { allApplications as all } from "../stores/applications";
import { IRouterContext } from "../stores/lib/router";

export const showApplications: IHandler = (context: IRouterContext) =>
  all()
    .then(() => context)
    .catch(() => {
      context.redirectUrl = parse(router.paths.Error404(), true);
      return context;
    });
