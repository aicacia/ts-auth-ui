import { some } from "@aicacia/core";
import { IHandler } from "@aicacia/router";
import { parse } from "url";
import { router } from "../lib";
import { isCurrentUserSignedIn } from "../stores/currentUser";
import { IRouterContext } from "../stores/lib/router";
import { setRedirectUrl, toggleSignInUpOpen } from "../stores/signInUp";

export const needsSignIn: IHandler = (context: IRouterContext) => {
  if (!isCurrentUserSignedIn()) {
    toggleSignInUpOpen();
    setRedirectUrl(some(context.url));
    context.redirectUrl = parse(router.paths.Error401(), true);
    return Promise.reject(context);
  } else {
    return Promise.resolve(context);
  }
};
