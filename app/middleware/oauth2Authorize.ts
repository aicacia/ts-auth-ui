import { IHandler } from "@aicacia/router";
import { parse } from "url";
import { router } from "../lib";
import { showApplication } from "../stores/applications";
import { IRouterContext } from "../stores/lib/router";

export const oauth2Authorize: IHandler = (context: IRouterContext) =>
  showApplication(+context.params.application_id)
    .then(() => context)
    .catch(() => {
      context.redirectUrl = parse(router.paths.Error404(), true);
      return context;
    });
