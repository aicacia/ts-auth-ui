import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Card, CardBody, Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { selectParam } from "../../../stores/lib/router";
import { Layout } from "../../shared/Layout";

export interface ICheckYourEmailStateProps {
  email: string;
}
export interface ICheckYourEmailFunctionProps {}
export interface ICheckYourEmailImplProps
  extends ICheckYourEmailProps,
    ICheckYourEmailStateProps,
    ICheckYourEmailFunctionProps {}

export interface ICheckYourEmailProps {}
export interface ICheckYourEmailState {}

const CheckYourEmailConnect = connect<
  ICheckYourEmailStateProps,
  ICheckYourEmailFunctionProps,
  ICheckYourEmailProps
>(
  (state, ownProps) => ({
    email: selectParam(state, "email") as string
  }),
  (state, ownProps, stateProps) => ({})
);

class CheckYourEmailImpl extends React.PureComponent<
  ICheckYourEmailImplProps,
  ICheckYourEmailState
> {
  render() {
    return (
      <Layout>
        <Container>
          <Card>
            <CardBody>
              <h3 className="text-center">
                <FormattedMessage
                  id="signInUp.check_your_inbox"
                  values={{ email: this.props.email }}
                />
              </h3>
            </CardBody>
          </Card>
        </Container>
      </Layout>
    );
  }
}

export const CheckYourEmail = CheckYourEmailConnect(CheckYourEmailImpl);
