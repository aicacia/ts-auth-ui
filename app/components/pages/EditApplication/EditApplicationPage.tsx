import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";
import { Page } from "../../lib/Page";

export const EditApplicationPage = injectIntl(({ intl }) => (
  <Async
    promise={import("./EditApplication")}
    onSuccess={({ EditApplication }) => (
      <Page>
        <Helmet>
          <title>
            {intl.formatMessage({ id: "app.page.editApplication" })}
          </title>
        </Helmet>
        <EditApplication />
      </Page>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
