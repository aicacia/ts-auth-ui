import { IInjectedFormProps } from "@aicacia/state-forms";
import Octicon, { ArrowLeft } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Container, Row } from "reactstrap";
import { router } from "../../../lib";
import { connect } from "../../../lib/state";
import {
  createDebounceUpdateApplication,
  IApplication,
  selectApplication
} from "../../../stores/applications";
import { selectParam } from "../../../stores/lib/router";
import { Layout } from "../../shared/Layout";
import {
  EditApplicationForm,
  IEditApplicationFormValues
} from "./EditApplicationForm";

export interface IEditApplicationStateProps {
  application: Record<IApplication>;
}
export interface IEditApplicationFunctionProps {}
export interface IEditApplicationImplProps
  extends IEditApplicationProps,
    IEditApplicationStateProps,
    IEditApplicationFunctionProps {}

export interface IEditApplicationProps {}
export interface IEditApplicationState {}

const EditApplicationConnect = connect<
  IEditApplicationStateProps,
  IEditApplicationFunctionProps,
  IEditApplicationProps
>(
  (state, ownProps) => ({
    application: selectApplication(
      state,
      +selectParam(state, "application_id")
    ).unwrap()
  }),
  (state, ownProps, stateProps) => ({})
);

class EditApplicationImpl extends React.PureComponent<
  IEditApplicationImplProps,
  IEditApplicationState
> {
  debounceUpdateApplication = createDebounceUpdateApplication();

  onUpdate = (props: IInjectedFormProps<IEditApplicationFormValues>) => {
    const formData = props.getFormData();

    this.debounceUpdateApplication(updateApplication =>
      updateApplication(this.props.application.get("id"), formData.toJS())
    );
  };
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Button href={router.paths.Applications()}>
              <Octicon icon={ArrowLeft} />
            </Button>
          </Row>
          <hr />
          <EditApplicationForm
            defaults={this.props.application.toJS()}
            onFormChangeValid={this.onUpdate}
          />
        </Container>
      </Layout>
    );
  }
}

export const EditApplication = EditApplicationConnect(EditApplicationImpl);
