import { none, Option } from "@aicacia/core";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Form, FormGroup } from "reactstrap";
import { injectForm } from "../../../stores/lib/forms";
import { TextField } from "../../shared/forms/TextField";

export interface IEditApplicationFormValues {
  name: string;
  home_url: string;
  privacy_policy_url: string;
  terms_url: string;
  logo_url: string;
}

export interface IEditApplicationFormProps
  extends IInjectedFormProps<IEditApplicationFormValues> {}
export interface IEditApplicationFormState {
  loading: boolean;
  error: Option<string>;
}

const EditApplicationFormInjectForm = injectForm<IEditApplicationFormValues>({
  changeset: changeset =>
    changeset.validateRequired([
      "name",
      "home_url",
      "privacy_policy_url",
      "terms_url",
      "logo_url"
    ])
});

class EditApplicationFormImpl extends React.PureComponent<
  IEditApplicationFormProps,
  IEditApplicationFormState
> {
  constructor(props: IEditApplicationFormProps) {
    super(props);

    this.state = {
      loading: false,
      error: none()
    };
  }
  render() {
    const { Field } = this.props;

    return (
      <Form>
        <Field
          name="name"
          label={<FormattedMessage id="applications.name" />}
          Component={TextField}
        />
        <Field
          name="home_url"
          label={<FormattedMessage id="applications.homeUrl" />}
          Component={TextField}
        />
        <Field
          name="privacy_policy_url"
          label={<FormattedMessage id="applications.privacyPolicyUrl" />}
          Component={TextField}
        />
        <Field
          name="terms_url"
          label={<FormattedMessage id="applications.termsUrl" />}
          Component={TextField}
        />
        <Field
          name="logo_url"
          label={<FormattedMessage id="applications.logoUrl" />}
          Component={TextField}
        />
        <FormGroup>
          {this.state.error
            .map(error => (
              <div className="alert alert-danger mt-4">{error}</div>
            ))
            .unwrapOr(null as any)}
        </FormGroup>
      </Form>
    );
  }
}

export const EditApplicationForm = EditApplicationFormInjectForm(
  EditApplicationFormImpl
);
