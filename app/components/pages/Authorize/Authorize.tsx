import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardImg,
  Col,
  Container,
  Row
} from "reactstrap";
import { Layout } from "../../../components/shared/Layout";
import { connect } from "../../../lib/state";
import { IApplication, selectApplication } from "../../../stores/applications";
import { selectParam } from "../../../stores/lib/router";

export interface IAuthorizeStateProps {
  applicationId: number;
  oauth2CredentialId: number;
  application: Record<IApplication>;
}
export interface IAuthorizeFunctionProps {}
export interface IAuthorizeImplProps
  extends IAuthorizeProps,
    IAuthorizeStateProps,
    IAuthorizeFunctionProps {}

export interface IAuthorizeProps {}
export interface IAuthorizeState {}

const AuthorizeConnect = connect<
  IAuthorizeStateProps,
  IAuthorizeFunctionProps,
  IAuthorizeProps
>(
  (state, ownProps) => {
    const applicationId = +selectParam(state, "application_id"),
      oauth2CredentialId = +selectParam(state, "oauth2_credential_id");

    return {
      applicationId,
      oauth2CredentialId,
      application: selectApplication(state, applicationId).unwrap()
    };
  },
  (state, ownProps, stateProps) => ({})
);

class AuthorizeImpl extends React.PureComponent<
  IAuthorizeImplProps,
  IAuthorizeState
> {
  onConfirm = () => {};
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardImg src={this.props.application.get("logo_url")} alt=" " />
                <CardHeader>
                  <h1>{this.props.application.get("name")}</h1>
                </CardHeader>
                <CardBody>
                  <div>
                    <FormattedMessage id="applications.homeUrl" />
                    <Button
                      color="link"
                      target="_blank"
                      href={this.props.application.get("home_url")}
                    >
                      {this.props.application.get("home_url")}
                    </Button>
                  </div>
                  <div>
                    <FormattedMessage id="applications.privacyPolicyUrl" />
                    <Button
                      color="link"
                      target="_blank"
                      href={this.props.application.get("privacy_policy_url")}
                    >
                      {this.props.application.get("privacy_policy_url")}
                    </Button>
                  </div>
                  <div>
                    <FormattedMessage id="applications.termsUrl" />
                    <Button
                      color="link"
                      target="_blank"
                      href={this.props.application.get("terms_url")}
                    >
                      {this.props.application.get("terms_url")}
                    </Button>
                  </div>
                </CardBody>
                <CardFooter>
                  <Button color="primary" onClick={this.onConfirm}>
                    <FormattedMessage id="oauth2.authorize" />
                  </Button>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </Layout>
    );
  }
}

export const Authorize = AuthorizeConnect(AuthorizeImpl);
