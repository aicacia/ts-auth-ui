import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";
import { Page } from "../../lib/Page";

export const AuthorizePage = injectIntl(({ intl }) => (
  <Async
    promise={import("./Authorize")}
    onSuccess={({ Authorize }) => (
      <Page>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.authorize" })}</title>
        </Helmet>
        <Authorize />
      </Page>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
