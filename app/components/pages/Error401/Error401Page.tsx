import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const Error401Page = injectIntl(({ intl }) => (
  <Async
    promise={import("./Error401")}
    onSuccess={({ Error401 }) => (
      <div>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.error401" })}</title>
        </Helmet>
        <Error401 />
      </div>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
