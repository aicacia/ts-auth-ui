import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";

interface IError401StateProps {}
interface IError401FunctionProps {}
interface IError401ImplProps
  extends IError401StateProps,
    IError401FunctionProps {}

export interface IError401Props {}
export interface IError401State {}

const Error401Connect = connect<
  IError401StateProps,
  IError401FunctionProps,
  IError401ImplProps
>(
  state => ({}),
  (state, ownProps, stateProps) => ({})
);

class Error401Impl extends React.PureComponent<
  IError401ImplProps,
  IError401State
> {
  render() {
    return (
      <Layout>
        <Container>
          <h1>
            <FormattedMessage id="error.401.name" />
          </h1>
          <p>
            <FormattedMessage id="error.401.message" />
          </p>
        </Container>
      </Layout>
    );
  }
}

export const Error401 = Error401Connect(Error401Impl);
