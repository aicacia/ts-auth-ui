import { none, Option, some } from "@aicacia/core";
import Octicon, { Pencil, Plus, Trashcan } from "@primer/octicons-react";
import { List, Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Col, Container, Row, Table } from "reactstrap";
import { location, router } from "../../../lib";
import { connect } from "../../../lib/state";
import {
  createApplication,
  IApplication,
  selectApplications
} from "../../../stores/applications";
import { selectCurrentUser } from "../../../stores/currentUser";
import { Layout } from "../../shared/Layout";
import { Application } from "./Application";
import { DeleteApplication } from "./DeleteApplication";

export interface IApplicationsStateProps {
  applications: List<Record<IApplication>>;
}
export interface IApplicationsFunctionProps {}
export interface IApplicationsImplProps
  extends IApplicationsProps,
    IApplicationsStateProps,
    IApplicationsFunctionProps {}

export interface IApplicationsProps {}
export interface IApplicationsState {
  creating: boolean;
  deleteApplication: Option<Record<IApplication>>;
}

const ApplicationsConnect = connect<
  IApplicationsStateProps,
  IApplicationsFunctionProps,
  IApplicationsProps
>(
  (state, ownProps) => ({
    applications: selectApplications(state, selectCurrentUser(state).get("id"))
  }),
  (state, ownProps, stateProps) => ({})
);

class ApplicationsImpl extends React.PureComponent<
  IApplicationsImplProps,
  IApplicationsState
> {
  constructor(props: IApplicationsImplProps) {
    super(props);

    this.state = {
      creating: false,
      deleteApplication: none()
    };
  }
  onCreate = () => {
    createApplication({
      name: "Application"
    })
      .then(application => {
        location.set(router.paths.EditApplication(application.get("id")));
      })
      .catch(() => {
        this.setState({
          creating: false
        });
      });
  };
  createOnDelete = (application: Record<IApplication>) => () => {
    this.setState({ deleteApplication: some(application) });
  };
  onCloseDelete = () => {
    this.setState({
      deleteApplication: none()
    });
  };
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col className="col-auto mr-auto"></Col>
            <Col className="col-auto">
              <Button
                color="success"
                onClick={this.onCreate}
                disabled={this.state.creating}
              >
                <Octicon icon={Plus} />
              </Button>
            </Col>
          </Row>
          <Row>
            {this.props.applications.map(application => (
              <Col key={application.get("id")}>
                <Application
                  application={application}
                  onDelete={this.createOnDelete(application)}
                />
              </Col>
            ))}
          </Row>
          <DeleteApplication
            application={this.state.deleteApplication}
            onClose={this.onCloseDelete}
          />
        </Container>
      </Layout>
    );
  }
}

export const Applications = ApplicationsConnect(ApplicationsImpl);
