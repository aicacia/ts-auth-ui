import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { Page } from "../../lib/Page";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const ApplicationsPage = injectIntl(({ intl }) => (
    <Async
        promise={import("./Applications")}
        onSuccess={({ Applications }) => (
            <Page>
                <Helmet>
                    <title>
                        {intl.formatMessage({ id: "app.page.applications" })}
                    </title>
                </Helmet>
                <Applications />
            </Page>
        )}
        onError={error => <JSError error={error} />}
        onPending={() => <Loading />}
    />
));
