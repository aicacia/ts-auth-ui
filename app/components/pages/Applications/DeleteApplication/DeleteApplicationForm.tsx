import { IInjectedFormProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Form } from "reactstrap";
import { injectForm } from "../../../../stores/lib/forms";
import { TextField } from "../../../shared/forms/TextField";

export interface IDeleteApplicationFormValues {
  name: string;
}

export interface IDeleteApplicationFormProps
  extends IInjectedFormProps<IDeleteApplicationFormValues> {}
export interface IDeleteApplicationFormState {}

const DeleteApplicationFormInjectForm = injectForm<
  IDeleteApplicationFormValues
>({
  changeset: changeset => changeset.validateRequired(["name"])
});

class DeleteApplicationFormImpl extends React.PureComponent<
  IDeleteApplicationFormProps,
  IDeleteApplicationFormState
> {
  render() {
    const { Field } = this.props;

    return (
      <Form>
        <Field
          name="name"
          label={<FormattedMessage id="applications.type_application_name" />}
          Component={TextField}
        />
      </Form>
    );
  }
}

export const DeleteApplicationForm = DeleteApplicationFormInjectForm(
  DeleteApplicationFormImpl
);
