import { Option } from "@aicacia/core";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage, injectIntl, IntlShape } from "react-intl";
import {
  Button,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row
} from "reactstrap";
import {
  deleteApplication,
  IApplication
} from "../../../../stores/applications";
import {
  DeleteApplicationForm,
  IDeleteApplicationFormValues
} from "./DeleteApplicationForm";

export interface IDeleteApplicationProps {
  intl: IntlShape;
  application: Option<Record<IApplication>>;
  onClose(): void;
}
export interface IDeleteApplicationState {
  valid: boolean;
  deleting: boolean;
}

class DeleteApplicationImpl extends React.PureComponent<
  IDeleteApplicationProps,
  IDeleteApplicationState
> {
  state = { valid: false, deleting: false };
  onDelete = () =>
    this.props.application.map(application => {
      this.setState({ deleting: true });
      deleteApplication(application.get("id")).then(() => {
        this.setState({ deleting: false });
        this.props.onClose();
      });
    });
  onFormChangeValid = (
    props: IInjectedFormProps<IDeleteApplicationFormValues>
  ) => {
    this.props.application.map(application => {
      this.setState({
        valid: props.getFormData().get("name") === application.get("name")
      });
    });
  };
  render() {
    return (
      <Modal
        isOpen={this.props.application.isSome()}
        toggle={this.props.onClose}
      >
        <ModalHeader toggle={this.props.onClose}>
          <FormattedMessage
            id="applications.confirm_delete_name"
            values={{
              name: this.props.application
                .map(application => application.get("name"))
                .unwrapOr("")
            }}
          />
        </ModalHeader>
        <ModalBody>
          <DeleteApplicationForm onFormChangeValid={this.onFormChangeValid} />
        </ModalBody>
        <ModalFooter>
          <Row>
            <Col>
              <Button
                color="danger"
                disabled={!this.state.valid || this.state.deleting}
                onClick={this.onDelete}
              >
                <FormattedMessage id="applications.delete" />
              </Button>
            </Col>
            <Col>
              <Button onClick={this.props.onClose}>
                <FormattedMessage id="applications.cancel" />
              </Button>
            </Col>
          </Row>
        </ModalFooter>
      </Modal>
    );
  }
}

export const DeleteApplication = injectIntl(DeleteApplicationImpl);
