import Octicon, {
  Clippy,
  Eye,
  EyeClosed,
  Pencil,
  Trashcan
} from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardImg,
  Input,
  InputGroup
} from "reactstrap";
import { router } from "../../../lib";
import { copyToClipboard } from "../../../lib/utils";
import { IApplication } from "../../../stores/applications";

export interface IApplicationProps {
  application: Record<IApplication>;
  onDelete(): void;
}
export interface IApplicationState {
  passwordShown: boolean;
}

export class Application extends React.PureComponent<
  IApplicationProps,
  IApplicationState
> {
  state: IApplicationState = {
    passwordShown: false
  };

  togglePassword = () =>
    this.setState({ passwordShown: !this.state.passwordShown });
  render() {
    return (
      <Card>
        <CardImg src={this.props.application.get("logo_url")} alt=" " />
        <CardHeader>
          <h1>{this.props.application.get("name")}</h1>
        </CardHeader>
        <CardBody>
          <div>
            <FormattedMessage id="applications.homeUrl" />
            <Button
              color="link"
              target="_blank"
              href={this.props.application.get("home_url")}
            >
              {this.props.application.get("home_url")}
            </Button>
          </div>
          <div>
            <FormattedMessage id="applications.privacyPolicyUrl" />
            <Button
              color="link"
              target="_blank"
              href={this.props.application.get("privacy_policy_url")}
            >
              {this.props.application.get("privacy_policy_url")}
            </Button>
          </div>
          <div>
            <FormattedMessage id="applications.termsUrl" />
            <Button
              color="link"
              target="_blank"
              href={this.props.application.get("terms_url")}
            >
              {this.props.application.get("terms_url")}
            </Button>
          </div>
        </CardBody>
        <CardFooter>
          <Button
            color="primary"
            href={router.paths.EditApplication(
              this.props.application.get("id")
            )}
          >
            <Octicon icon={Pencil} />
          </Button>
          <Button onClick={this.props.onDelete} color="danger">
            <Octicon icon={Trashcan} />
          </Button>
        </CardFooter>
      </Card>
    );
  }
}
