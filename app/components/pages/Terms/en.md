## Terms of Use

AUTH.AICACIA.COM WEBSITE TERMS OF USE AND COPYRIGHT NOTICE

The following terms and conditions (the "Agreement") govern all use of the aicacia-auth.com website (the "Site") and the products and services available on or at the Site (taken together with the Site, the "Service"). The Service is owned and operated by Aicacia Auth. The Service is offered subject to your (the "CurrentUser's") acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies and procedures that may be available from time to time on the Site by Aicacia Auth. BY USING OR ACCESSING ANY PART OF THE SERVICE, YOU (the "CurrentUser") AGREE TO ALL OF THE TERMS AND CONDITIONS CONTAINED HEREIN; IF YOU NOT AGREE, DO NOT USE OR ACCESS THE SERVICE.
Aicacia Auth reserves the right, at its sole discretion, to modify or replace any of the terms or conditions of this Agreement at any time. It is CurrentUser's responsibility to check this Agreement periodically for changes. CurrentUser's continued use of the Service following the posting of any changes to this Agreement constitutes acceptance of those changes.

ACCESS.

Subject to the terms and conditions of this Agreement, the software and services provided in connection with the Service are solely for CurrentUser's own personal use, and not for the use or benefit of any third party. Aicacia Auth may change, suspend or discontinue the Services, including the availability of any feature, database or content, at any time. Aicacia Auth may also impose limits on certain features and services or restrict CurrentUser's access to parts or all of the Services without notice or liability. Aicacia Auth reserves the right, at its discretion, to modify this Agreement at any time by posting a notice on the Site or by sending CurrentUser a notice via email or postal mail. CurrentUser shall be responsible for reviewing and becoming familiar with any such modifications. Use of the Service by CurrentUser following such notification constitutes CurrentUser's acceptance of the terms and conditions of this Agreement as modified.
CurrentUser certifies to Aicacia Auth that if CurrentUser is an individual (i.e., not a corporation) CurrentUser is at least 13 years of age (provided that, if CurrentUser is located in a state in which 13 is under the age of majority, then CurrentUser represents he/she has the legal consent of a legal guardian to enter this Agreement). CurrentUser also certifies that CurrentUser is legally permitted to use the Service and takes full responsibility for the selection and use of the Service. This Agreement is void where prohibited by law, and the right to access the Service is revoked in such jurisdictions.

FEES AND PAYMENT.

The basic Service is free to users, but if CurrentUser elects to upgrade to a subscription membership or any of our other paid features, the following terms below shall apply. Aicacia Auth reserves the right to change its prices at any time.

#### A. Billing and Payment.

Aicacia Auth bills users through an online account for use of the Service. CurrentUser agrees to pay Aicacia Auth all applicable Service fees using this online account. All fees are exclusive of applicable taxes (e.g. sales, use, or value-added tax), unless otherwise stated, and CurrentUser is solely responsible for the payment of any such taxes that may be imposed on your use of the Service. Aicacia Auth may correct any billing errors or mistakes that it makes even if it has already requested or received payment. Aicacia Auth reserves the right to change its prices for certain Service features - and to institute new charges at any time - upon notice to you, which may be sent by email or posted on the Site. CurrentUser agrees to maintain current, complete and accurate information for CurrentUser's billing account. In addition, CurrentUser authorizes Aicacia Auth to obtain updated or replacement expiration dates and card numbers for CurrentUser's credit or debit card as provided by CurrentUser's credit or debit card issuer. All fees are final and nonrefundable (including in the event any features or functions of any service that CurrentUser has subscribed to are changed, modified, diminished or removed). CurrentUser shall be responsible for any applicable foreign transaction fees charged by CurrentUser's bank.

#### B. Automatic Renewal

CurrentUser's subscription to the Service will continue indefinitely until cancelled by CurrentUser (via the cancellation mechanisms provided on the Site). After CurrentUser's initial subscription period, and again after any subsequent subscription period, CurrentUser's subscription will automatically continue for an additional equivalent period, at the price CurrentUser agreed to when subscribing. CurrentUser agrees that CurrentUser's account will be subject to this automatic renewal feature. If CurrentUser does not wish CurrentUser's account to renew automatically, or if CurrentUser wants to change or terminate CurrentUser's subscription, please log in and go to the Account > Billing page. If CurrentUser cancels a subscription, CurrentUser may use such subscription until the end of then-current subscription term and the subscription will not be renewed thereafter. However, CurrentUser will not be eligible for a prorated refund of any portion of the subscription fee paid for the then-current subscription period.
By subscribing, CurrentUser authorizes Aicacia Auth to charge CurrentUser's credit card, debit card or other payment method at such time and again at the beginning of any subsequent subscription period, including any sales or similar taxes imposed on CurrentUser's subscription payments. Upon the renewal of your subscription, if Aicacia Auth does not receive payment, CurrentUser agrees that Aicacia Auth may either terminate or suspend CurrentUser's subscription and continue to attempt to charge CurrentUser's payment method provider until payment is received (upon receipt of payment, CurrentUser's account will be activated and for purposes of automatic renewal, CurrentUser's new subscription commitment period will begin as of the day payment was received). If CurrentUser does not terminate CurrentUser's subscription and/or if CurrentUser continues to use the Service, CurrentUser agrees that Aicacia Auth is authorized to charge the payment method in CurrentUser's Service account. Aicacia Auth may also seek payment directly from CurrentUser. Charges may be payable in advance, in arrears, per usage, or as otherwise specified when CurrentUser initially subscribes to the Service.

#### C. Promotions

Any free trial or other promotion that provides subscriber-level access to the Service must be used within the specified time of the trial. CurrentUser must cancel CurrentUser's subscription before the end of the trial period in order to avoid being charged a subscription fee.

SITE CONTENT.

CurrentUser agrees that all content and materials (collectively, "Content") delivered via the Service or otherwise made available by Aicacia Auth at the Site are protected by copyrights, trademarks, service marks, patents, trade secrets or other proprietary rights and laws. Except as expressly authorized by Aicacia Auth in writing, CurrentUser agrees not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create derivative works from such materials or content. However, CurrentUser may print or download a reasonable number of copies of the materials or content at this Site for CurrentUser's own informational purposes; provided, that CurrentUser retain all copyright and other proprietary notices contained therein. Reproducing, copying or distributing any content, materials or design elements on the Site for any other purpose is strictly prohibited without the express prior written permission of Aicacia Auth.

Use of the content or materials for any purpose not expressly permitted in this Agreement is prohibited. Any rights not expressly granted herein are reserved.

USER CONTENT.

The Sites and Service may permit the submission of communications (such as commentary on other users content) and other content submitted by CurrentUser ("CurrentUser Submission(s)") and the hosting, sharing, and/or publishing of such CurrentUser Submission(s).

CurrentUser shall be solely responsible for CurrentUser's CurrentUser Submission(s) and the consequences of posting or publishing them. CurrentUser agrees that Aicacia Auth has no liability with respect to any CurrentUser Submission(s), including, without limitation, CurrentUser's CurrentUser Submission(s), and CurrentUser hereby irrevocably release Aicacia Auth and its officers and directors, employees, agents, representatives and affiliates, from any and all liability arising out of or relating to CurrentUser Submission(s) or any part thereof.

By submitting the CurrentUser Submission(s) to Aicacia Auth, or displaying, publishing, or otherwise posting any content on or through the Sites or the Service, CurrentUser hereby does and shall grant Aicacia Auth a worldwide, non-exclusive, perpetual, royalty-free, fully paid, sublicensable and transferable license to reproduce, distribute, modify (such as by editing or otherwise creating derivatives), display, and perform the CurrentUser Submission(s) in accordance with Aicacia Auth’s CurrentUser Submission policies and CurrentUser's Site settings. CurrentUser agrees not to contest any modifications made by Aicacia Auth and hereby waives any claims with respect thereto. For clarity, the foregoing license grant to Aicacia Auth does not affect CurrentUser's other ownership or license rights in your CurrentUser Submission(s), including the right to grant additional licenses to the material in CurrentUser's CurrentUser Submission(s). CurrentUser Submission(s) may be withdrawn by written request to us to delete content.

In connection with CurrentUser's CurrentUser Submission(s), CurrentUser affirms, represents, and warrants that: (i) CurrentUser owns or has the necessary licenses, rights, consents, and permissions to use and authorize Aicacia Auth to use each and every image and sound contained in each such CurrentUser Submission and to enable inclusion and use of such CurrentUser Submission(s) in the manner contemplated by the Sites, the Service and this Agreement; (ii) CurrentUser has the written consent, release, and/or permission of each and every identifiable individual person in the CurrentUser Submission to use the name or likeness of each and every such identifiable individual person to enable inclusion and use of the CurrentUser Submission(s) in the manner contemplated by the Sites, the Service and this Agreement; and (iii) the posting of your CurrentUser Submission on or through the Sites or the Service or otherwise does not violate the privacy rights, publicity rights, intellectual property rights, contract rights, or any other rights of any person or entity. CurrentUser agrees to pay all royalties, fees, and other monies owing any person or entity by reason of any content posted by CurrentUser to or through the Sites or the Service.

CurrentUser further agrees that CurrentUser will not: (i) publish falsehoods or misrepresentations that could damage Aicacia Auth or any third party; (ii) submit material that is unlawful, obscene, lewd, defamatory, libelous, threatening, pornographic, harassing, hateful, racially or ethnically offensive, excessively violent, or encourages conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate or objectionable; (iii) post advertisements or solicitations of business; (iv) impersonate another person; (v) submit material that is copyrighted, protected by trade secret or otherwise subject to third party intellectual property or proprietary rights, including privacy and publicity right, unless you are the owner of such rights or have permission from their rightful owner to post the material and to grant Aicacia Auth all of the license rights granted herein; or (vi) submit material that is unsuitable for minors in any country.

Aicacia Auth does not endorse any CurrentUser Submission or any opinion, recommendation, or advice expressed therein, and Aicacia Auth expressly disclaims any and all responsibility or liability in connection with CurrentUser Submission(s). Aicacia Auth reserves the right to, in its sole discretion, remove any CurrentUser Submission(s) at any time (with or without notice).

SOFTWARE.

With respect to any software CurrentUser is authorized by Aicacia Auth to download via the Service, Aicacia Auth grants CurrentUser a personal, nontransferable, nonsublicensable, nonexclusive license to use such software solely for CurrentUser's personal, non-commercial use, and only in accordance with the written instructions/directions (if any) provided by Aicacia Auth. CurrentUser shall not, nor permit anyone else to, directly or indirectly: (i) copy, modify, or distribute the software; (ii) reverse engineer, disassemble, decompile or otherwise attempt to discover the source code or structure, sequence and organization of all or any part of the software (except that this restriction shall not apply to the limited extent restrictions on reverse engineering prohibited by applicable local law); or (iii) rent, lease, or use the software for timesharing or service bureau purposes, or otherwise use the software for any commercial purpose.

RESTRICTIONS.

CurrentUser shall not use any "deep-link", "page-scrape", "robot", "spider" or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Service or any Content, or in any way reproduce or circumvent the navigational structure or presentation of the Service or any Content, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the Service. Aicacia Auth reserves the right to bar any such activity.

CurrentUser shall not attempt to gain unauthorized access to any portion or feature of the Service, or any other systems or networks connected to the Service or to any Aicacia Auth server, or to any of the services offered on or through the Service, by hacking, password "mining", or any other illegitimate means.

CurrentUser shall not probe, scan or test the vulnerability of the Service or any network connected to the Service, nor breach the security or authentication measures on the Service or any network connected to the Service. CurrentUser shall not reverse look-up, trace or seek to trace any information on any other currentUser of or visitor to the Service, or any other customer of Aicacia Auth, including any Aicacia Auth account not owned by CurrentUser, to its source, or exploit the Service or any service or information made available or offered by or through the Service, in any way where the purpose is to reveal any information, including but not limited to personal identification or information, other than CurrentUser's own information, as provided for by the Service.

CurrentUser shall not take any action that imposes an unreasonable or disproportionately large load on the infrastructure of the Service or Aicacia Auth’s systems or networks, or any systems or networks connected to the Service or to Aicacia Auth.

CurrentUser shall not use any device, software or routine to interfere or attempt to interfere with the proper working of the Service or any transaction being conducted on the Service, or with any other person's use of the Service.

CurrentUser shall not forge headers or otherwise manipulate identifiers in order to disguise the origin of any message or transmittal CurrentUser sends to Aicacia Auth on or through the Service. CurrentUser shall not, in connection with the Service, pretend (e.g. through impersonation) that CurrentUser is any other individual or entity.

CurrentUser shall not use the Service or any Content for any purpose that is unlawful or prohibited by this Agreement, or that infringes the rights of Aicacia Auth or others.

REGISTRATION; SECURITY.

As a condition to using certain features of the Service, CurrentUser may be required to register with Aicacia Auth and select a password and CurrentUser name ("Aicacia Auth CurrentUser ID"). CurrentUser shall provide Aicacia Auth with accurate, complete, and updated registration information. Failure to do so shall constitute a breach of this Agreement, which may result in immediate termination of CurrentUser's account. CurrentUser may not (i) select or use as a Aicacia Auth CurrentUser ID a name of another person with the intent to impersonate that person; or (ii) use as a Aicacia Auth CurrentUser ID a name subject to any rights of a person other than CurrentUser without appropriate authorization. Aicacia Auth reserves the right, in its discretion, to cancel or refuse registration of an Aicacia Auth CurrentUser ID. CurrentUser shall be responsible for maintaining the confidentiality of CurrentUser's Aicacia Auth password and other account information.

THIRD PARTY SITES.

The Site may permit CurrentUser to link to other websites or resources on the Internet, and other websites or resources may contain links to the Site. These other websites are not under Aicacia Auth’s control, and CurrentUser acknowledges that Aicacia Auth is not responsible or liable for the content, functions, accuracy, legality, appropriateness or any other aspect of such websites or resources. The inclusion of any such link does not imply endorsement by Aicacia Auth. CurrentUser further acknowledges and agrees that Aicacia Auth shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any content, goods, information, or services available on or through any such website or resource.

INDEMNIFICATION.

CurrentUser is responsible for all of its activity in connection with the Service. CurrentUser shall defend, indemnify, and hold harmless Aicacia Auth, its affiliates, and each of its and its affiliates' employees, contractors, directors, suppliers and representatives from all liabilities, claims, and expenses, including reasonable attorneys' fees, that arise from (i) CurrentUser's use or misuse of the Service; (ii) CurrentUser's access to any part of the Service, (iii) any of CurrentUser's CurrentUser Submission(s), or (iv) otherwise from CurrentUser's violation of this Agreement.

WARRANTY DISCLAIMER.

THE SERVICE (INCLUDING, WITHOUT LIMITATION, THE SITE AND ANY SOFTWARE) IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. STEMBORD MAKES NO WARRANTY THAT (I) THE SERVICE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR (II) THE RESULTS OF USING THE SERVICE (OR ANY USER SUBMISSION(S)) WILL MEET USER'S REQUIREMENTS. USER'S USE OF THE SERVICE IS SOLELY AT USER'S OWN RISK.

LIMITATION OF LIABILITY.

IN NO EVENT SHALL STEMBORD, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, VENDORS OR SUPPLIERS BE LIABLE UNDER CONTRACT, TORT, STRICT LIABILITY, NEGLIGENCE OR ANY OTHER LEGAL THEORY WITH RESPECT TO THE SERVICE (OR ANY CONTENT, PROGRAMS OR INFORMATION AVAILABLE THROUGH THE SERVICE): (I) FOR ANY LOST PROFITS OR SPECIAL, INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER, SUBSTITUTE GOODS OR SERVICES (HOWEVER ARISING), (II) FOR ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE (REGARDLESS OF THE SOURCE OF ORIGINATION), (III) FOR ANY ERRORS OR OMISSIONS IN ANY CONTENT OR INFORMATION OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF USER'S USE OF ANY CONTENT OR INFORMATION POSTED, EMAILED, TRANSMITTED OR OTHERWISE MADE AVAILABLE AT OR THROUGH THE SERVICE, OR (IV) FOR ANY DIRECT DAMAGES IN EXCESS OF (IN THE AGGREGATE) \$500.00 (U.S.). IN ADDITION, STEMBORD SHALL NOT BE LIABLE FOR ANY LOSS OR LIABILITY RESULTING, DIRECTLY OR INDIRECTLY, FROM USER'S INABILITY TO ACCESS OR OTHERWISE USE THE SITE (INCLUDING, WITHOUT LIMITATION, ANY DELAYS OR INTERRUPTIONS DUE TO ELECTRONIC OR MECHANICAL EQUIPMENT FAILURES, DENIAL OF SERVICE ATTACKS, PROCESSING FAILURES, TELECOMMUNICATIONS OR INTERNET PROBLEMS OR UTILITY FAILURES).

TERMINATION.

Aicacia Auth may terminate CurrentUser's access to all or any part of the Service at any time, with or without cause, effective upon notice thereof to CurrentUser (provided that, if Aicacia Auth determines there may be an immediate threat to Aicacia Auth, it may terminate such access without notice).

Upon termination notice from Aicacia Auth, CurrentUser will (i) no longer access (or attempt to access) the Service and (ii) delete all software (and copies thereof) provided in connection with the Service. All provisions of this Agreement that by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers and limitations of liability.

EXPORT AND TRADE CONTROLS.

CurrentUser agrees not to import, export, re-export, or transfer, directly or indirectly, any part of the Service or any information provided on or pursuant to the Service except in full compliance with all United States, foreign and other applicable laws and regulations.

PRIVACY.

Aicacia Auth’s current privacy policy is available at on aicacia-auth.com (the "Privacy Policy"), which is incorporated by this reference.

MEMBER DISPUTES.

CurrentUser is solely responsible for your interactions with other Service users. Aicacia Auth reserves the right, but has no obligation, to monitor disputes between CurrentUser and other Service users. If CurrentUser has a dispute with one or more users of the Service, CurrentUser shall and hereby does release Aicacia Auth (and its officers, directors, agents, subsidiaries, joint ventures and employees) from claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, arising out of or in any way connected with such disputes. If CurrentUser is a California resident, CurrentUser waives California Civil Code §1542, which states: "A general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which if known by him must have materially affected his settlement with the debtor."

COPYRIGHT.

All content included on the Site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of Aicacia Auth or its content suppliers and is protected by United States and international copyright laws. The compilation of all content on the Site is the exclusive property of Aicacia Auth and is protected by U.S. and international copyright laws. All software used on (or provided through) the Site is the property of Aicacia Auth or its software suppliers and is protected by United States and international copyright laws.

ELECTRONIC COMMUNICATIONS.

When CurrentUser visits the Site or sends e-mails to Aicacia Auth, CurrentUser is communicating with Aicacia Auth electronically. CurrentUser hereby consents to receive communications from Aicacia Auth electronically. Aicacia Auth will communicate with CurrentUser by e-mail or by posting notices on the Site. CurrentUser agrees that all agreements, notices, disclosures and other communications that Aicacia Auth provides to CurrentUser electronically satisfy any legal requirements that such communications be in writing.

MISCELLANEOUS.

The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights hereunder. Aicacia Auth shall not be liable for any failure to perform its obligations hereunder where such failure results from any cause beyond Aicacia Auth’ reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation (including "line-noise" interference). If any provision of this Agreement is found to be unenforceable or invalid, that provision shall be limited or eliminated to the minimum extent necessary so that this Agreement shall otherwise remain in full force and effect and enforceable. This Agreement is not assignable, transferable or sublicensable by CurrentUser except with Aicacia Auth' prior written consent. Aicacia Auth may transfer, assign or delegate this Agreement and its rights and obligations without consent. This Agreement shall be governed by and construed in accordance with the laws of the state of Pennsylvania, as if made within Pennsylvania between two residents thereof, and the parties submit to the exclusive jurisdiction and venue of the state and Federal courts located in San Francisco, California. Notwithstanding the foregoing sentence, (but without limiting either party's right to seek injunctive or other equitable relief immediately, at any time, in any court of competent jurisdiction), any disputes arising with respect to this Agreement shall be settled by arbitration in accordance with the rules and procedures of the Judicial Arbitration and Mediation Service, Inc. ("JAMS"). The arbitrator shall be selected by joint agreement of the parties. In the event the parties cannot agree on an arbitrator within thirty (30) days of the initiating party providing the other party with written notice that it plans to seek arbitration, the parties shall each select an arbitrator affiliated with JAMS, which arbitrators shall jointly select a third such arbitrator to resolve the dispute. The written decision of the arbitrator shall be final and binding on the parties and enforceable in any court. The arbitration proceeding shall take place in San Francisco, California, using the English language. Both parties agree that this Agreement is the complete and exclusive statement of the mutual understanding of the parties and supersedes and cancels all previous written and oral agreements, communications and other understandings relating to the subject matter of this Agreement, and that all modifications must be in a writing signed by both parties, except as otherwise provided herein. No agency, partnership, joint venture, or employment is created as a result of this Agreement and CurrentUser does not have any authority of any kind to bind Aicacia Auth in any respect whatsoever.

DIGITAL MILLENNIUM COPYRIGHT ACT NOTICE.

Aicacia Auth has adopted the following general policy toward copyright infringement in accordance with the Digital Millennium Copyright Act. The address of Aicacia Auth' Designated Agent to Receive Notification of Claimed Infringement ("Designated Agent") is listed at the end of this policy.

It is Aicacia Auth' policy to (1) block access to or remove content that it believes in good faith to be copyrighted material that has been illegally copied and distributed by any of our advertisers, affiliates, content providers, members or users; and (2) remove and discontinue service to repeat offenders.

#### A. Procedure for Reporting Copyright Infringements:

If CurrentUser believes that content residing on or accessible through the Aicacia Auth Service infringes a copyright, please send a notice of copyright infringement containing the following information to the Designated Agent listed below:

1. A physical or electronic signature of a person authorized to act on behalf of the owner of the copyright that has been allegedly infringed;
2. Identification of works or materials being infringed;
3. Identification of the content that is claimed to be infringing including information regarding the location of the content that the copyright owner seeks to have removed, with sufficient detail so that Aicacia Auth is capable of finding and verifying its existence;
4. Contact information about the notifier including address, telephone number and, if available, e-mail address;
5. A statement that the notifier has a good faith belief that the content is not authorized by the copyright owner, its agent, or the law; and
6. A statement made under penalty of perjury that the information provided is accurate and the notifying party is authorized to make the complaint on behalf of the copyright owner.

#### B. Once Proper Bona Fide Infringement Notification is Received by the Designated Agent:

It is Aicacia Auth' policy:

1. to remove or disable access to the infringing content;
2. to notify the content provider, member or currentUser that it has removed or disabled access to the content;
3. that repeat offenders will have the infringing content removed from the system; and
4. to terminate such content provider's, member's or currentUser's access to the service.

#### C. Procedure to Supply a Counter-Notice to the Designated Agent:

If the content provider, member or currentUser believes that the content that was removed or to which access was disabled is either not infringing, or the content provider, member or currentUser believes that it has the right to post and use such content from the copyright owner, the copyright owner's agent or pursuant to the law, the content provider, member or currentUser must send a counter-notice containing the following information to the Designated Agent listed below:

1. A physical or electronic signature of the content provider, member or currentUser;
2. Identification of the content that has been removed or to which access has been disabled and the location at which the content appeared before it was removed or disabled;
3. A statement that the content provider, member or currentUser has a good faith belief that the content was removed or disabled as a result of mistake or a misidentification of the content; and
4. Content provider's, member's or currentUser's name, address, telephone number, and, if available, email address and a statement that such person or entity consents to the jurisdiction of the Federal Court for the judicial district in which the content provider's, member's or currentUser's address is located, or if the content provider's, member's or currentUser's address is located outside the United States, for any judicial district in which Aicacia Auth is located, and that such person or entity will accept service of process from the person who provided notification of the alleged infringement.

If a counter-notice is received by the Designated Agent, Aicacia Auth may send a copy of the counter-notice to the original complaining party informing that person that it may replace the removed content or cease disabling it in 10 business days. Unless the copyright owner files an action seeking a court order against the content provider, member or currentUser, the removed content may be replaced, or access to it restored, in 10 to 14 business days or more after receipt of the counter-notice, at Aicacia Auth’ discretion.

Please contact Aicacia Auth’ Designated Agent to Receive Notification of Claimed Infringement.
