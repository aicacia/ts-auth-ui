import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const PrivacyPolicyPage = injectIntl(({ intl }) => (
  <Async
    promise={import("./PrivacyPolicy")}
    onSuccess={({ PrivacyPolicy }) => (
      <div>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.privacyPolicy" })}</title>
        </Helmet>
        <PrivacyPolicy />
      </div>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
