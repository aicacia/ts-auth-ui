import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";

export interface IError500StateProps {}
export interface IError500FunctionProps {}
export interface IError500ImplProps
  extends IError500Props,
    IError500StateProps,
    IError500FunctionProps {}

export interface IError500Props {}
export interface IError500State {}

const Error500Connect = connect<
  IError500StateProps,
  IError500FunctionProps,
  IError500Props
>(
  (state, ownProps) => ({}),
  (state, ownProps, stateProps) => ({})
);

class Error500Impl extends React.PureComponent<
  IError500ImplProps,
  IError500State
> {
  constructor(props: IError500ImplProps) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <Layout>
        <Container>
          <h1>
            <FormattedMessage id="error.500.name" />
          </h1>
          <p>
            <FormattedMessage id="error.500.message" />
          </p>
        </Container>
      </Layout>
    );
  }
}

export const Error500 = Error500Connect(Error500Impl);
