import { IInputProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormGroup, Input, Label } from "reactstrap";
import { FormFieldErrorMessage } from "../FormFieldErrorMessage";

export type ICheckboxProps = IInputProps<boolean> & {
  label?: React.ReactNode;
};
export interface ICheckboxState {}

export class Checkbox extends React.PureComponent<
  ICheckboxProps,
  ICheckboxState
> {
  onChange = (e: React.ChangeEvent) => {
    this.props.change((e.target as any).checked);
  };
  render() {
    const {
      error,
      errors,
      value,
      focus,
      change,
      onChange,
      ...props
    } = this.props;

    const input = (
      <Input
        type="checkbox"
        checked={value}
        {...props}
        invalid={error}
        onChange={this.onChange}
      />
    );

    return (
      <FormGroup check>
        {this.props.label ? (
          <Label check>
            {input}
            {this.props.label}
          </Label>
        ) : (
          input
        )}
        <FormFieldErrorMessage errors={errors} />
      </FormGroup>
    );
  }
}
