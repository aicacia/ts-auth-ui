import { IInputProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormGroup, Input, Label } from "reactstrap";
import { InputType } from "reactstrap/lib/Input";
import { FormFieldErrorMessage } from "../FormFieldErrorMessage";

export type ITextFieldProps = IInputProps<string> & {
  type?: InputType;
  placeholder?: string;
  disabled?: boolean;
  label?: React.ReactChild;
};
export interface ITextFieldState {}

export class TextField extends React.PureComponent<
  ITextFieldProps,
  ITextFieldState
> {
  render() {
    const {
      error,
      errors,
      focus,
      onChange,
      change,
      label,
      type,
      ...props
    } = this.props;

    return (
      <FormGroup>
        {label && <Label>{this.props.label}</Label>}
        <Input {...props} type={type} onChange={onChange} invalid={error} />
        <FormFieldErrorMessage errors={errors} />
      </FormGroup>
    );
  }
}
