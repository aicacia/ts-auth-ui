import { Record } from "immutable";
import { default as React } from "react";
import Gravatar from "react-gravatar";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { location, router } from "../../../lib";
import {
  Email,
  ICurrentUser,
  signCurrentUserOut
} from "../../../stores/currentUser";
import { toggleSignInUpOpen } from "../../../stores/signInUp";

export interface IAccountProps {
  isCurrentUserSignedIn: boolean;
  currentUser: Record<ICurrentUser>;
}
export interface IAccountState {
  anchorEl?: HTMLInputElement;
  open: boolean;
}

export class Account extends React.PureComponent<IAccountProps, IAccountState> {
  constructor(props: IAccountProps) {
    super(props);

    this.state = {
      open: false
    };
  }
  toggleMenu = (e: React.MouseEvent<HTMLInputElement>) => {
    this.setState({
      anchorEl: e.target as HTMLInputElement,
      open: !this.state.open
    });
  };
  onSignOut = () => {
    signCurrentUserOut().then(() => {
      location.set(router.paths.Index());
    });
  };
  render() {
    if (this.props.isCurrentUserSignedIn) {
      return (
        <Dropdown
          isOpen={this.state.open}
          toggle={this.toggleMenu as any}
          direction="down"
        >
          <DropdownToggle color="secondary" split id="header-dropdown">
            <Gravatar
              className="mr-2"
              email={this.props.currentUser.get("email", Email).get("email")}
              size={24}
            />
          </DropdownToggle>

          <DropdownMenu right>
            <DropdownItem href={router.paths.Applications()}>
              <FormattedMessage id="app.page.applications" />
            </DropdownItem>
            <DropdownItem onClick={this.onSignOut}>
              <FormattedMessage id="signInUp.signOut" />
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      );
    } else {
      return (
        <Button onClick={toggleSignInUpOpen} color="primary">
          <FormattedMessage id="signInUp.signInUp" />
        </Button>
      );
    }
  }
}
