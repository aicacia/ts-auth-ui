import { Option } from "@aicacia/core";
import { default as React } from "react";
import { FormattedMessage, injectIntl, IntlShape } from "react-intl";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { UrlWithParsedQuery } from "url";
import { connect } from "../../../../lib/state";
import {
  selectRedirectUrl,
  selectSignInUpOpen,
  toggleSignInUpOpen
} from "../../../../stores/signInUp";
import { JSError } from "../../../lib/JSError";
import { SignInForm } from "./SignInForm";
import { SignUpForm } from "./SignUpForm";

interface ISignInUpStateProps {
  open: boolean;
  redirectUrl: Option<UrlWithParsedQuery>;
}

interface ISignInUpFunctionProps {}

interface ISignInUpImplProps
  extends ISignInUpStateProps,
    ISignInUpFunctionProps {}

export interface ISignInUpProps {}

export interface ISignInUpState {
  error?: Error;
  signIn: boolean;
}

const SignInUpConnect = connect<
  ISignInUpStateProps,
  ISignInUpFunctionProps,
  ISignInUpProps
>(
  (state, ownProps) => ({
    redirectUrl: selectRedirectUrl(state),
    open: selectSignInUpOpen(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class SignInUpImpl extends React.PureComponent<
  ISignInUpImplProps,
  ISignInUpState
> {
  constructor(props: ISignInUpImplProps) {
    super(props);

    this.state = {
      signIn: true
    };
  }
  toggleSignIn = () => this.setState({ signIn: !this.state.signIn });
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={toggleSignInUpOpen}>
        <ModalHeader toggle={toggleSignInUpOpen}>
          {this.state.signIn ? (
            <FormattedMessage id="signInUp.signIn" />
          ) : (
            <FormattedMessage id="signInUp.signUp" />
          )}
        </ModalHeader>
        <ModalBody>
          <p>
            {this.state.signIn ? (
              <FormattedMessage
                id="signInUp.notMember"
                values={{
                  link: (
                    <Button onClick={this.toggleSignIn}>
                      <FormattedMessage id="signInUp.signUp" />
                    </Button>
                  )
                }}
              />
            ) : (
              <FormattedMessage
                id="signInUp.member"
                values={{
                  link: (
                    <Button onClick={this.toggleSignIn}>
                      <FormattedMessage id="signInUp.signIn" />
                    </Button>
                  )
                }}
              />
            )}
          </p>
          {this.state.signIn ? (
            <SignInForm redirectUrl={this.props.redirectUrl} />
          ) : (
            <SignUpForm redirectUrl={this.props.redirectUrl} />
          )}
          {this.state.error && <JSError error={this.state.error} />}
        </ModalBody>
      </Modal>
    );
  }
}

export const SignInUp = SignInUpConnect(SignInUpImpl);
