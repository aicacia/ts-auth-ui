import { none, Option, some } from "@aicacia/core";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form, FormGroup } from "reactstrap";
import { format, UrlWithParsedQuery } from "url";
import { location } from "../../../../lib";
import { signCurrentUserInWithEmailPassword } from "../../../../stores/currentUser";
import { injectForm } from "../../../../stores/lib/forms";
import {
  setRedirectUrl,
  toggleSignInUpOpen
} from "../../../../stores/signInUp";
import { TextField } from "../../../shared/forms/TextField";

export interface ISignInFormValues {
  email: string;
  password: string;
}

export interface ISignInFormProps
  extends IInjectedFormProps<ISignInFormValues> {
  redirectUrl: Option<UrlWithParsedQuery>;
}
export interface ISignInFormState {
  loading: boolean;
  error: Option<string>;
}

const SignInFormInjectForm = injectForm<ISignInFormValues>({
  changeset: changeset =>
    changeset
      .validateLength("password", { ">=": 6 })
      .validateRequired(["email", "password"])
});

class SignInFormImpl extends React.PureComponent<
  ISignInFormProps,
  ISignInFormState
> {
  state: ISignInFormState = {
    loading: false,
    error: none()
  };
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData(),
        email = formData.get("email") as string,
        password = formData.get("password") as string;

      this.setState({
        loading: true
      });

      signCurrentUserInWithEmailPassword(email, password)
        .then(() => {
          this.props.redirectUrl.map(redirectUrl => {
            setRedirectUrl(none());
            location.set(format(redirectUrl));
          });
          toggleSignInUpOpen();
        })
        .catch(error => {
          console.error(error);
          this.setState({
            loading: false,
            error: some(error.response.data.error)
          });
        });
    }
  };
  render() {
    const { Field } = this.props;

    return (
      <Form onSubmit={this.onSubmit}>
        <Field
          name="email"
          label={<FormattedMessage id="signInUp.email" />}
          Component={TextField}
        />
        <Field
          name="password"
          type="password"
          label={<FormattedMessage id="signInUp.password" />}
          Component={TextField}
        />
        <FormGroup>
          <Button
            type="submit"
            disabled={!this.props.valid || this.state.loading}
            onSubmit={this.onSubmit}
          >
            <FormattedMessage id="signInUp.submit" />
          </Button>

          {this.state.error
            .map(error => (
              <div className="alert alert-danger mt-4">{error}</div>
            ))
            .unwrapOr(null as any)}
        </FormGroup>
      </Form>
    );
  }
}

export const SignInForm = SignInFormInjectForm(SignInFormImpl);
