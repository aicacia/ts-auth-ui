import { Changeset } from "@aicacia/changeset";
import { none, Option, some } from "@aicacia/core";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form, FormGroup } from "reactstrap";
import { format, UrlWithParsedQuery } from "url";
import { location } from "../../../../lib";
import { signCurrentUserUpWithEmailPassword } from "../../../../stores/currentUser";
import { injectForm } from "../../../../stores/lib/forms";
import {
  setRedirectUrl,
  toggleSignInUpOpen
} from "../../../../stores/signInUp";
import { Checkbox } from "../../forms/Checkbox";
import { TextField } from "../../forms/TextField";

export interface ISignUpFormValues {
  email: string;
  password: string;
  password_confirmation: string;
  terms_and_conditions: boolean;
}

export interface ISignUpFormProps
  extends IInjectedFormProps<ISignUpFormValues> {
  redirectUrl: Option<UrlWithParsedQuery>;
}
export interface ISignUpFormState {
  loading: boolean;
  error: Option<string>;
}

const SignUpFormInjectForm = injectForm<ISignUpFormValues>({
  changeset: changeset =>
    validatePassword(
      changeset
        .validateLength("password", { ">=": 6 })
        .validateAcceptance("terms_and_conditions")
        .validateRequired([
          "email",
          "password",
          "password_confirmation",
          "terms_and_conditions"
        ])
    )
});

const validatePassword = (changeset: Changeset<ISignUpFormValues>) => {
  const password = changeset.getChange("password"),
    password_confirmation = changeset.getChange("password_confirmation");

  if (password !== password_confirmation) {
    return changeset.addError(
      "password_confirmation",
      "invalid_password_confirmation"
    );
  } else {
    return changeset;
  }
};

class SignUpFormImpl extends React.PureComponent<
  ISignUpFormProps,
  ISignUpFormState
> {
  constructor(props: ISignUpFormProps) {
    super(props);

    this.state = {
      loading: false,
      error: none()
    };
  }
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData(),
        email = formData.get("email") as string,
        password = formData.get("password") as string,
        password_confirmation = formData.get("password_confirmation") as string;

      this.setState({
        loading: true
      });

      signCurrentUserUpWithEmailPassword(email, password, password_confirmation)
        .then(() => {
          this.props.redirectUrl.map(redirectUrl => {
            setRedirectUrl(none());
            location.set(format(redirectUrl));
          });
          toggleSignInUpOpen();
        })
        .catch(error => {
          console.log(error);
          this.setState({
            loading: false,
            error: some(error.response.data.error)
          });
        });
    }
  };
  render() {
    const { Field } = this.props;

    return (
      <Form onSubmit={this.onSubmit}>
        <Field
          name="email"
          label={<FormattedMessage id="signInUp.email" />}
          Component={TextField}
        />
        <Field
          name="password"
          type="password"
          label={<FormattedMessage id="signInUp.password" />}
          Component={TextField}
        />
        <Field
          name="password_confirmation"
          type="password"
          label={<FormattedMessage id="signInUp.passwordConfirmation" />}
          Component={TextField}
        />
        <Field
          name="terms_and_conditions"
          label={
            <FormattedMessage
              id="signInUp.terms_and_conditions"
              values={{
                terms: (
                  <a href="/terms">
                    <FormattedMessage id="app.terms" />
                  </a>
                ),
                privacy_policy: (
                  <a href="/privacy_policy">
                    <FormattedMessage id="app.privacyPolicy" />
                  </a>
                )
              }}
            />
          }
          Component={Checkbox}
        />
        <FormGroup>
          <Button
            type="submit"
            disabled={!this.props.valid || this.state.loading}
            onSubmit={this.onSubmit}
          >
            <FormattedMessage id="signInUp.submit" />
          </Button>

          {this.state.error
            .map(error => (
              <div className="alert alert-danger mt-4">{error}</div>
            ))
            .unwrapOr(null as any)}
        </FormGroup>
      </Form>
    );
  }
}

export const SignUpForm = SignUpFormInjectForm(SignUpFormImpl);
