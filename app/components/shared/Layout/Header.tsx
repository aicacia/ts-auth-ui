import { Record } from "immutable";
import { default as React } from "react";
import { Nav, Navbar } from "reactstrap";
import { connect } from "../../../lib/state";
import {
  ICurrentUser,
  isCurrentUserSignedIn,
  selectCurrentUser
} from "../../../stores/currentUser";
import { selectHeight, selectWidth } from "../../../stores/lib/screenSize";
import { isSyncing } from "../../../stores/sync";
import { Sizes } from "../Sizes";
import { Account } from "./Account";
import { Brand } from "./Brand";
import { Footer } from "./Footer";
import { SignInUp } from "./SignInUp";

interface IHeaderStateProps {
  currentUser: Record<ICurrentUser>;
  width: number;
  height: number;
  isCurrentUserSignedIn: boolean;
  isSyncing: boolean;
}
interface IHeaderFunctionProps {}

interface IHeaderImplProps extends IHeaderStateProps, IHeaderFunctionProps {}

export interface IHeaderProps {}
export interface IHeaderState {}

const HeaderConnect = connect<
  IHeaderStateProps,
  IHeaderFunctionProps,
  IHeaderProps
>(
  (state, ownProps) => ({
    width: selectWidth(state),
    height: selectHeight(state),
    currentUser: selectCurrentUser(state),
    isCurrentUserSignedIn: isCurrentUserSignedIn(),
    isSyncing: isSyncing(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class HeaderImpl extends React.PureComponent<IHeaderImplProps, IHeaderState> {
  render() {
    return (
      <Sizes
        refs={["header", "footer"]}
        width={this.props.width}
        height={this.props.height}
      >
        {(sizes, refs) => (
          <>
            <div ref={refs.header as React.RefObject<HTMLDivElement>}>
              <Navbar color="white" light className="border-bottom">
                <Brand />
                <Nav className="mr-auto" />
                {this.props.isSyncing && (
                  <div
                    className="spinner-border text-secondary mr-2"
                    role="status"
                  >
                    <span className="sr-only">Loading...</span>
                  </div>
                )}
                <Account
                  currentUser={this.props.currentUser}
                  isCurrentUserSignedIn={this.props.isCurrentUserSignedIn}
                />
              </Navbar>
            </div>
            <div
              style={{
                overflowY: "scroll",
                height:
                  this.props.height -
                    (sizes.header.height + sizes.footer.height) || 0
              }}
            >
              <div className="mt-4 mb-4">{this.props.children}</div>
            </div>
            <SignInUp />
            <Footer
              refObject={refs.footer as React.RefObject<HTMLDivElement>}
            />
          </>
        )}
      </Sizes>
    );
  }
}

export const Header = HeaderConnect(HeaderImpl);
