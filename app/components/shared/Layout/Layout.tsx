import { Record } from "immutable";
import { default as React } from "react";
import { connect } from "../../../lib/state";
import {
  ICurrentUser,
  isCurrentUserSignedIn,
  selectCurrentUser
} from "../../../stores/currentUser";
import { Header } from "./Header";

interface ILayoutStateProps {
  currentUser: Record<ICurrentUser>;
  isCurrentUserSignedIn: boolean;
}
interface ILayoutFunctionProps {}

interface ILayoutImplProps extends ILayoutStateProps, ILayoutFunctionProps {}

export interface ILayoutProps {}
export interface ILayoutState {}

const LayoutConnect = connect<
  ILayoutStateProps,
  ILayoutFunctionProps,
  ILayoutProps
>(
  (state, ownProps) => ({
    currentUser: selectCurrentUser(state),
    isCurrentUserSignedIn: isCurrentUserSignedIn()
  }),
  (state, ownProps, stateProps) => ({})
);

class LayoutImpl extends React.PureComponent<ILayoutImplProps, ILayoutState> {
  render() {
    return <Header>{this.props.children}</Header>;
  }
}

export const Layout = LayoutConnect(LayoutImpl);
