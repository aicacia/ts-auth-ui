import { default as React } from "react";

export interface ILoadingProps {}
export interface ILoadingState {}

export class Loading extends React.PureComponent<ILoadingProps, ILoadingState> {
  render() {
    return (
      <div className="container h-100">
        <div className="row h-100 justify-content-center align-items-center">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      </div>
    );
  }
}
