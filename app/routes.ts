import { ApplicationsPage } from "./components/pages/Applications/ApplicationsPage";
import { AuthorizePage } from "./components/pages/Authorize/AuthorizePage";
import { CheckYourEmailPage } from "./components/pages/CheckYourEmail/CheckYourEmailPage";
import { EditApplicationPage } from "./components/pages/EditApplication/EditApplicationPage";
import { Error401Page } from "./components/pages/Error401/Error401Page";
import { Error404Page } from "./components/pages/Error404/Error404Page";
import { Error500Page } from "./components/pages/Error500/Error500Page";
import { IndexPage } from "./components/pages/Index/IndexPage";
import { PrivacyPolicyPage } from "./components/pages/PrivacyPolicy/PrivacyPolicyPage";
import { TermsPage } from "./components/pages/Terms/TermsPage";
import { renderApp } from "./lib/internal";
import { router } from "./lib/router";
import { autoSignIn } from "./middleware/autoSignIn";
import { confirmUserEmail } from "./middleware/confirmUserEmail";
import { needsSignIn } from "./middleware/needsSignIn";
import { oauth2Authorize } from "./middleware/oauth2Authorize";
import { showApplication } from "./middleware/showApplication";
import { showApplications } from "./middleware/showApplications";

const init = () => {
  router.use("/confirm-email/:confirmation_token", confirmUserEmail);
  router.use("/", autoSignIn);

  router.page("Error401", "/401", Error401Page);
  router.page("Error404", "/404", Error404Page);
  router.page("Error500", "/500", Error500Page);

  router.page("PrivacyPolicy", "/privacy-policy", PrivacyPolicyPage);
  router.page("Terms", "/terms", TermsPage);

  router.page("Index", "/", IndexPage);
  router.page("CheckYourEmail", "/check_your_email", CheckYourEmailPage);

  router.use("/", needsSignIn);

  /* DO NOT REMOVE THIS */
  router.with("/oauth2", oauth2 => {
    oauth2.with("/authorize", authorize => {
      authorize.use("/", oauth2Authorize);
      authorize.page("Authorize", "/", AuthorizePage);
    });
  });

  router.with("/applications", applications => {
    applications.use("/", showApplications);

    applications.page("Applications", "/", ApplicationsPage);

    applications.with("/:application_id", application => {
      application.use("/", showApplication);
      application.page("EditApplication", "/edit", EditApplicationPage);
    });
  });
};

init();

if (process.env.NODE_ENV !== "production") {
  if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
      router.clear();
      init();
      renderApp();
    });
  }
}
