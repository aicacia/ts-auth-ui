import "ts-polyfill";

import Axios from "axios";
import { location } from "./lib/location";

import { renderApp } from "./lib/internal";
import "./routes.ts";

import ReactGA from "react-ga";

Axios.defaults.baseURL = window.env.APP_API_URL;
Axios.defaults.headers.common.Accept = "application/json";
Axios.defaults.headers.common["Content-Type"] = "application/json";
Axios.defaults.withCredentials = true;

window.addEventListener("load", () => {
  ReactGA.initialize([
    {
      trackingId: window.env.APP_GA_TRACKING_ID,
      debug: window.env.APP_GA_DEBUG === "true",
      testMode: window.env.APP_GA_TEST_MODE === "true"
    }
  ]);
  location.init();
  renderApp();
});

if (process.env.NODE_ENV !== "production") {
  if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
      renderApp();
    });
  }
}
