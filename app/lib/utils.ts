export const copyToClipboard = (text: string) => {
  const textField = document.createElement("textarea");
  textField.innerHTML = text;
  textField.style.display = "hidden";
  document.body.appendChild(textField);
  textField.select();
  document.execCommand("copy");
  textField.remove();
};
