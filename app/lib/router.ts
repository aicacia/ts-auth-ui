import { none, Option } from "@aicacia/core";
import { Layer, Router as BaseRouter } from "@aicacia/router";
import { default as React } from "react";
import { IRouterContext, update } from "../stores/lib/router";

const COMPONENTS: { [state: string]: React.ComponentType } = {};

class Router extends BaseRouter {
  paths: { [key: string]: (...args: any[]) => string };

  constructor(path: string = "/", parent: Option<Layer> = none()) {
    super(path, parent);

    this.Router = Router;
    this.paths = {};
  }

  addPath(name: string, layer: Layer) {
    const root = this.getRoot() as Router;
    root.paths[name] = (...args: any[]) => layer.format(...args);
  }

  page(state: string, path: string, Component: React.ComponentType): Router {
    COMPONENTS[state] = Component;

    const route = this.route(path, (context: IRouterContext): void => {
      update(context, state);
    });

    this.addPath(state, route);

    return this;
  }

  with(path: string = "/", scope: (router: Router) => void): Router {
    const router = this.scope(path) as Router;
    scope(router);
    return router;
  }
}

export const router = new Router();

export const getComponent = (state: string) => COMPONENTS[state];
