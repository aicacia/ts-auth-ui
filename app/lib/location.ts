import { IHandler, Location } from "@aicacia/location";
import { createContext } from "@aicacia/router";
import { parse, UrlWithParsedQuery } from "url";
import { set } from "../stores/lib/router";
import { router } from "./router";

const handler: IHandler = (url: UrlWithParsedQuery) => {
  const context = createContext(url);

  set(context);

  return router.handle(context).then(
    () => {
      if (context.redirectUrl) {
        return Promise.reject(context.redirectUrl);
      } else if (!context.resolved) {
        return Promise.reject(parse(router.paths.Error404(), true));
      } else {
        return Promise.resolve(context.url);
      }
    },
    error => {
      if (error && context.redirectUrl) {
        return Promise.reject(context.redirectUrl);
      } else if (error) {
        console.error(error);
        return Promise.reject(parse(router.paths.Error500(), true));
      } else if (!context.resolved) {
        return Promise.reject(parse(router.paths.Error404(), true));
      } else {
        return Promise.reject(null);
      }
    }
  );
};

export const location = new Location(window, { html5Mode: true, handler });
