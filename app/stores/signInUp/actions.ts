import { Option, some } from "@aicacia/core";
import { UrlWithParsedQuery } from "url";
import { state } from "../../lib/state";
import { ISignInUpJSON, SignInUp, STORE_NAME } from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: ISignInUpJSON) =>
  SignInUp({
    open: json.open,
    redirectUrl: Option.from(json.redirectUrl)
  });

export const toggleSignInUpOpen = (): void => {
  store.updateState(state => state.set("open", !state.get("open", false)));
};

export const setRedirectUrl = (
  redirectUrl: Option<UrlWithParsedQuery>
): void => {
  store.updateState(state => state.set("redirectUrl", redirectUrl));
};
