import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectSignInUpOpen = (state: IState) =>
  state.get(STORE_NAME).get("open");

export const selectRedirectUrl = (state: IState) =>
  state.get(STORE_NAME).get("redirectUrl");
