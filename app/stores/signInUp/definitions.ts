import { none, Option } from "@aicacia/core";
import { Record } from "immutable";
import { UrlWithParsedQuery } from "url";

export interface ISignInUp {
  open: boolean;
  redirectUrl: Option<UrlWithParsedQuery>;
}

export const SignInUp = Record<ISignInUp>({
  open: false,
  redirectUrl: none()
});

export type ISignInUpJSON = ReturnType<Record<ISignInUp>["toJS"]>;

export const STORE_NAME = "signInUp";
export const INITIAL_STATE = SignInUp();
