import { IState } from "../../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectWidth = (state: IState): number =>
  state.get(STORE_NAME).get("width", window.innerWidth);

export const selectHeight = (state: IState): number =>
  state.get(STORE_NAME).get("height", window.innerHeight);
