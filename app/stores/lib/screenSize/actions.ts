import { debounce } from "@aicacia/debounce";
import { state } from "../../../lib/state";
import {
  IScreenSizeJSON,
  ScreenSize,
  STORE_NAME,
  TIMEOUT
} from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: IScreenSizeJSON) =>
  ScreenSize({
    width: json.width,
    height: json.height
  });

export const sync = debounce(() => {
  store.updateState(state =>
    state.set("width", window.innerWidth).set("height", window.innerHeight)
  );
}, TIMEOUT);

window.addEventListener("resize", sync);
window.addEventListener("orientationchange", sync);
