import { State, Store } from "@aicacia/state";
import * as cookies from "es-cookie";
import { fromJS, Map, Record } from "immutable";

export const locales = {
  en: import("../../../locales/en.json")
};

export type IExtractGeneric<Type> = Type extends Promise<infer X> ? X : any;
export type ISupporttedLocale = keyof typeof locales;
export type ILocaleMessages = {
  [K in ISupporttedLocale]: IExtractGeneric<(typeof locales)[K]>["default"];
};

export const LOCALE_TOKEN = "X-AicaciaAuth-Locale";
// TODO: get default fallback from navigator if in supportLocales()
export const DEFAULT_LOCALE =
  (cookies.get(LOCALE_TOKEN) as ISupporttedLocale) || "en";

cookies.set(LOCALE_TOKEN, DEFAULT_LOCALE);

export interface ILocales {
  locale: ISupporttedLocale;
  messages: Map<ISupporttedLocale, Map<string, string>>;
}

export const Locales = Record<ILocales>({
  locale: DEFAULT_LOCALE,
  messages: Map()
});

export const supportLocales: () => ISupporttedLocale[] = (() => {
  const keys: any[] = Object.keys(locales);
  return () => keys;
})();

export type ILocalesJSON = ReturnType<Record<ILocales>["toJS"]>;

export const STORE_NAME = "locales";
export const INITIAL_STATE = Locales();

export const loadLocale = (
  locale: ISupporttedLocale
): Promise<Map<string, string>> =>
  (locales[locale] as Promise<any>).then(
    (localeMessages: ILocaleMessages[typeof locale]) =>
      fromJS(localeMessages) as Map<string, string>
  );

if (process.env.NODE_ENV !== "production") {
  if ((module as any).hot) {
    const reloadLocales = (
      store: Store<
        State<{ [STORE_NAME]: typeof INITIAL_STATE }>,
        typeof INITIAL_STATE
      >
    ) =>
      Promise.all<{ locale: ISupporttedLocale; messages: Map<string, string> }>(
        supportLocales().map(locale =>
          loadLocale(locale).then(messages => ({ locale, messages }))
        )
      ).then(results =>
        store.updateState(state =>
          results.reduce(
            (state, { locale, messages }) =>
              state.set(
                "messages",
                state.get("messages").set(locale, messages)
              ),
            state
          )
        )
      );

    ((module as any).hot as any).accept(() => {
      reloadLocales((window as any).__DEV_STATE.getStore(STORE_NAME)).then(
        () => (window as any).__DEV_RENDER_APP
      );
    });
  }
}
