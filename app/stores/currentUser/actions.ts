import { debounce } from "@aicacia/debounce";
import Axios, { AxiosResponse } from "axios";
import * as cookies from "es-cookie";
import { List, Record } from "immutable";
import ReactGA from "react-ga";
import { location, router } from "../../lib";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync/actions";
import {
  CurrentUser,
  Email,
  ICurrentUser,
  ICurrentUserJSON,
  STORE_NAME,
  USER_TOKEN
} from "./definitions";
import { isCurrentUserSignedIn } from "./selectors";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: ICurrentUserJSON) =>
  CurrentUser({
    ...json,
    inserted_at: new Date(json.inserted_at),
    updated_at: new Date(json.updated_at),
    email: Email(json.email),
    emails: List(json.emails.map(Email))
  });

const signInCurrentUser = (response: AxiosResponse) => {
  const currentUser = store.fromJSON(response.data),
    email = currentUser.get("email", Email()),
    tokenKey = currentUser.get("token");

  ReactGA.set({ userId: currentUser.get("id") });

  cookies.set(USER_TOKEN, tokenKey);
  Axios.defaults.headers.common.Authorization = tokenKey;

  store.updateState(() => currentUser);

  if (!email.get("confirmed")) {
    location.set(
      `${router.paths.CheckYourEmail()}?email=${encodeURIComponent(
        email.get("email")
      )}`
    );
  }

  return currentUser;
};

export const autoCurrentUserSignIn = () => {
  const token = cookies.get(USER_TOKEN);

  if (token) {
    return signCurrentUserInWithToken(token);
  } else {
    return Promise.reject(new Error("Token not available"));
  }
};

export const signCurrentUserInWithToken = (token: string) => {
  if (!isCurrentUserSignedIn()) {
    return Axios.get("/user/current-user", {
      headers: {
        Authorization: token
      }
    }).then(signInCurrentUser);
  } else {
    return Promise.resolve(store.getState());
  }
};

export const signCurrentUserInWithEmailPassword = (
  email: string,
  password: string
) => {
  if (!isCurrentUserSignedIn()) {
    return Axios.post("/auth/sign-in/identity/callback", {
      email,
      password
    }).then(signInCurrentUser);
  } else {
    return Promise.resolve(store.getState());
  }
};

export const signCurrentUserUpWithEmailPassword = (
  email: string,
  password: string,
  password_confirmation: string
) => {
  if (!isCurrentUserSignedIn()) {
    return Axios.post("/auth/sign-up/identity/callback", {
      email,
      password,
      password_confirmation
    }).then(signInCurrentUser);
  } else {
    return Promise.resolve(store.getState());
  }
};

export const confirmCurrentUserEmail = (confirmation_token: string) =>
  Axios.post(`/user/confirm-email`, {
    confirmation_token
  }).then(signInCurrentUser);

export const signCurrentUserOut = (): Promise<Record<ICurrentUser>> => {
  if (isCurrentUserSignedIn()) {
    const promise = Axios.delete("/user/sign-out"),
      currentUser = CurrentUser();

    cookies.remove(USER_TOKEN);
    delete Axios.defaults.headers.common.Authorization;

    store.updateState(() => currentUser);

    return promise.then(() => currentUser);
  } else {
    return Promise.resolve(store.getState());
  }
};

export const updateCurrentUser = (
  user: Partial<ICurrentUser>
): Promise<Record<ICurrentUser>> =>
  Axios.patch<ICurrentUser>("/user", {
    user
  }).then(response => {
    const currentUser = store.fromJSON(response.data as any);
    store.updateState(() => currentUser);
    return currentUser;
  });

export const createDebounceUpdate = (): ((
  callbackFn: (updateFn: typeof updateCurrentUser) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateCurrentUser), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });
