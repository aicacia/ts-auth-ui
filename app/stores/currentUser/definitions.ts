import { List, Map, Record } from "immutable";

export interface IEmail {
  id: number;
  email: string;
  primary: boolean;
  confirmed: boolean;
}

export const Email = Record<IEmail>({
  id: 0,
  email: "",
  primary: false,
  confirmed: false
});

export interface ICurrentUser {
  id: string;
  token: string;
  email: Record<IEmail>;
  emails: List<Record<IEmail>>;
  inserted_at: Date;
  updated_at: Date;
}

export const CurrentUser = Record<ICurrentUser>({
  id: "",
  token: "",
  email: Email(),
  emails: List(),
  inserted_at: new Date(),
  updated_at: new Date()
});

export type ICurrentUserJSON = ReturnType<Record<ICurrentUser>["toJS"]>;

export const INITIAL_STATE = CurrentUser();
export const STORE_NAME = "currentUser";
export const USER_TOKEN = "X-AicaciaAuth-CurrentUser-Token";
