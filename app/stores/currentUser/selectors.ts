import Axios from "axios";
import { Record } from "immutable";
import { IState } from "../../lib/state";
import { ICurrentUser, STORE_NAME } from "./definitions";

export const selectCurrentUser = (state: IState): Record<ICurrentUser> =>
  state.get(STORE_NAME);

export const isCurrentUserSignedIn = () =>
  !!Axios.defaults.headers.common.Authorization;
