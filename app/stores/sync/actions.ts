import { state } from "../../lib/state";
import { ISyncJSON, STORE_NAME, Sync } from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: ISyncJSON) =>
  Sync({
    syncing: json.syncing
  });

export const startSync = () =>
  store.updateState(state => state.update("syncing", count => count + 1));
export const endSync = () =>
  store.updateState(state => state.update("syncing", count => count - 1));

export const syncWrap = <T>(promise: Promise<T>): Promise<T> => {
  startSync();
  return promise
    .then(result => {
      endSync();
      return result;
    })
    .catch(e => {
      endSync();
      throw e;
    });
};
