import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectApplications = (state: IState, ownerId: string) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .filter(application => application.get("owner_id") === ownerId)
    .toList();

export const selectApplication = (state: IState, applicationId: number) =>
  Option.from(state.get(STORE_NAME).byId.get(applicationId));
