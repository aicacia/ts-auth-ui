import { Map, Record } from "immutable";

export interface IApplication {
  id: number;
  owner_id: string;
  name: string;
  home_url: string;
  privacy_policy_url: string;
  terms_url: string;
  logo_url: string;
  inserted_at: string;
  updated_at: string;
}

export const Application = Record<IApplication>({
  id: 0,
  owner_id: "",
  name: "",
  home_url: "",
  privacy_policy_url: "",
  terms_url: "",
  logo_url: "",
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface IApplications {
  byId: Map<number, Record<IApplication>>;
}

export const Applications = Record<IApplications>({
  byId: Map()
});

export type IApplicationJSON = ReturnType<Record<IApplication>["toJS"]>;
export interface IApplicationsJSON {
  byId: { [id: number]: IApplicationJSON };
}

export const APPLICATIONS = "applications";
export const APPLICATIONS_URL = `/${APPLICATIONS}`;
export const APPLICATION_ID = "application_id";
export const APPLICATIONS_BY_ID_URL = `${APPLICATIONS_URL}/${APPLICATION_ID}`;

export const applicationsUrl = () => APPLICATIONS_URL;
export const applicationByIdUrl = (applicationId: number) =>
  `${applicationsUrl()}/${applicationId}`;

export const INITIAL_STATE = Applications();
export const STORE_NAME = "applications";
