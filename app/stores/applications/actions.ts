import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync/actions";
import {
  Application,
  applicationByIdUrl,
  Applications,
  applicationsUrl,
  IApplication,
  IApplicationJSON,
  IApplicationsJSON,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: IJSON) => {
  const applicationsJSON: IApplicationsJSON = json as any;

  return Applications({
    byId: Object.values(applicationsJSON.byId).reduce(
      (byId, applicationJSON) =>
        byId.set(applicationJSON.id, Application(applicationJSON)),
      Map<number, Record<IApplication>>()
    )
  });
};

export const allApplications = () =>
  Axios.get<IApplication[]>(applicationsUrl()).then(response => {
    const applications = List(response.data.map(Application));

    store.updateState(state =>
      state.set(
        "byId",
        applications.reduce(
          (byId, application) => byId.set(application.get("id"), application),
          state.get("byId")
        )
      )
    );

    return applications;
  });

export const showApplication = (applicationId: number) => {
  const application = store
    .getState()
    .get("byId")
    .get(applicationId);

  if (application) {
    return Promise.resolve(application);
  } else {
    return Axios.get<IApplication>(applicationByIdUrl(applicationId)).then(
      response => {
        const application = Application(response.data);

        store.updateState(state =>
          state.set(
            "byId",
            state.get("byId").set(application.get("id"), application)
          )
        );

        return application;
      }
    );
  }
};

export const createApplication = (application: Partial<IApplicationJSON>) =>
  Axios.post<IApplication>(applicationsUrl(), application).then(response => {
    const application = Application(response.data);

    store.updateState(state =>
      state.set(
        "byId",
        state.get("byId").set(application.get("id"), application)
      )
    );

    return application;
  });

export const updateApplication = (
  applicationId: number,
  application: Partial<IApplicationJSON>
) =>
  Axios.patch<IApplication>(
    applicationByIdUrl(applicationId),
    application
  ).then(response => {
    const application = Application(response.data);

    store.updateState(state =>
      state.set(
        "byId",
        state.get("byId").set(application.get("id"), application)
      )
    );

    return application;
  });

export const createDebounceUpdateApplication = (): ((
  callbackFn: (updateFn: typeof updateApplication) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateApplication), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });

export const deleteApplication = (
  applicationId: number
): Promise<Option<Record<IApplication>>> => {
  const application = store
    .getState()
    .get("byId")
    .get(applicationId);

  if (application) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(application.get("id")))
    );

    return Axios.delete<IApplication>(applicationByIdUrl(applicationId))
      .then(() => some(application))
      .catch(() => {
        store.updateState(state =>
          state.set(
            "byId",
            state.get("byId").set(application.get("id"), application)
          )
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
