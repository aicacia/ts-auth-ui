import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync/actions";
import {
  IOAuth2Credential,
  IOAuth2CredentialJSON,
  IOAuth2CredentialsJSON,
  OAuth2Credential,
  oauth2CredentialByIdUrl,
  OAuth2Credentials,
  oauth2CredentialsUrl,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: IJSON) => {
  const oauth2_credentialsJSON: IOAuth2CredentialsJSON = json as any;

  return OAuth2Credentials({
    byId: Object.values(oauth2_credentialsJSON.byId).reduce(
      (byId, oauth2_credentialJSON) =>
        byId.set(
          oauth2_credentialJSON.id,
          OAuth2Credential(oauth2_credentialJSON)
        ),
      Map<number, Record<IOAuth2Credential>>()
    )
  });
};

export const allOAuth2Credentials = (applicationId: number) =>
  Axios.get<IOAuth2Credential[]>(oauth2CredentialsUrl(applicationId)).then(
    response => {
      const oauth2_credentials = List(response.data.map(OAuth2Credential));

      store.updateState(state =>
        state.set(
          "byId",
          oauth2_credentials.reduce(
            (byId, oauth2_credential) =>
              byId.set(oauth2_credential.get("id"), oauth2_credential),
            state.get("byId")
          )
        )
      );

      return oauth2_credentials;
    }
  );

export const showOAuth2Credential = (
  applicationId: number,
  oauth2CredentialId: number
) => {
  const oauth2_credential = store
    .getState()
    .get("byId")
    .get(oauth2CredentialId);

  if (oauth2_credential) {
    return Promise.resolve(oauth2_credential);
  } else {
    return Axios.get<IOAuth2Credential>(
      oauth2CredentialByIdUrl(applicationId, oauth2CredentialId)
    ).then(response => {
      const oauth2_credential = OAuth2Credential(response.data);

      store.updateState(state =>
        state.set(
          "byId",
          state.get("byId").set(oauth2_credential.get("id"), oauth2_credential)
        )
      );

      return oauth2_credential;
    });
  }
};

export const createOAuth2Credential = (
  applicationId: number,
  oauth2Credential: Partial<IOAuth2CredentialJSON>
) =>
  Axios.post<IOAuth2Credential>(
    oauth2CredentialsUrl(applicationId),
    oauth2Credential
  ).then(response => {
    const oauth2_credential = OAuth2Credential(response.data);

    store.updateState(state =>
      state.set(
        "byId",
        state.get("byId").set(oauth2_credential.get("id"), oauth2_credential)
      )
    );

    return oauth2_credential;
  });

export const updateOAuth2Credential = (
  applicationId: number,
  oauth2CredentialId: number,
  oauth2Credential: Partial<IOAuth2CredentialJSON>
) =>
  Axios.patch<IOAuth2Credential>(
    oauth2CredentialByIdUrl(applicationId, oauth2CredentialId),
    oauth2Credential
  ).then(response => {
    const oauth2_credential = OAuth2Credential(response.data);

    store.updateState(state =>
      state.set(
        "byId",
        state.get("byId").set(oauth2_credential.get("id"), oauth2_credential)
      )
    );

    return oauth2_credential;
  });

export const createDebounceUpdateOAuth2Credential = (): ((
  callbackFn: (updateFn: typeof updateOAuth2Credential) => void
) => void) =>
  debounce(
    onDebounce => onDebounce(updateOAuth2Credential),
    UPDATE_DEBOUNCE_MS,
    {
      before: startSync,
      after: endSync
    }
  );

export const deleteOAuth2Credential = (
  applicationId: number,
  oauth2CredentialId: number
): Promise<Option<Record<IOAuth2Credential>>> => {
  const oauth2_credential = store
    .getState()
    .get("byId")
    .get(oauth2CredentialId);

  if (oauth2_credential) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(oauth2_credential.get("id")))
    );

    return Axios.delete<IOAuth2Credential>(
      oauth2CredentialByIdUrl(applicationId, oauth2CredentialId)
    )
      .then(() => some(oauth2_credential))
      .catch(() => {
        store.updateState(state =>
          state.set(
            "byId",
            state
              .get("byId")
              .set(oauth2_credential.get("id"), oauth2_credential)
          )
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
