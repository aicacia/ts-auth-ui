import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectOAuth2Credentials = (state: IState, applicationId: number) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .filter(
      oauth2Credential =>
        oauth2Credential.get("application_id") === applicationId
    )
    .toList();

export const selectOAuth2Credential = (
  state: IState,
  oauth2CredentialId: number
) => {
  console.log(state.get(STORE_NAME).byId.toJS(), oauth2CredentialId);
  return Option.from(state.get(STORE_NAME).byId.get(oauth2CredentialId));
};
