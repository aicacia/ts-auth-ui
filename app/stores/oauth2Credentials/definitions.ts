import { List, Map, Record } from "immutable";
import {
  applicationByIdUrl,
  APPLICATIONS_BY_ID_URL
} from "../applications/definitions";

export interface IOAuth2Credential {
  id: number;
  application_id: number;
  name: string;
  client_id: string;
  client_secret: string;
  origins: List<string>;
  redirects: List<string>;
  inserted_at: string;
  updated_at: string;
}

export const OAuth2Credential = Record<IOAuth2Credential>({
  id: 0,
  application_id: 0,
  name: "",
  client_id: "",
  client_secret: "",
  origins: List(),
  redirects: List(),
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface IOAuth2Credentials {
  byId: Map<number, Record<IOAuth2Credential>>;
}

export const OAuth2Credentials = Record<IOAuth2Credentials>({
  byId: Map()
});

export type IOAuth2CredentialJSON = ReturnType<
  Record<IOAuth2Credential>["toJS"]
>;
export interface IOAuth2CredentialsJSON {
  byId: { [key: number]: IOAuth2CredentialJSON };
}

export const OAUTH2_CREDENTIALS = "oauth2-credentials";
export const OAUTH2_CREDENTIALS_URL = `${APPLICATIONS_BY_ID_URL}/${OAUTH2_CREDENTIALS}`;
export const OAUTH2_CREDENTIAL_ID = "oauth2_credential_id";
export const OAUTH2_CREDENTIALS_BY_ID_URL = `${OAUTH2_CREDENTIALS_URL}/${OAUTH2_CREDENTIAL_ID}`;

export const oauth2CredentialsUrl = (applicationId: number) =>
  `${applicationByIdUrl(applicationId)}/${OAUTH2_CREDENTIALS}`;
export const oauth2CredentialByIdUrl = (
  applicationId: number,
  oauth2CredentialId: number
) => `${oauth2CredentialsUrl(applicationId)}/${oauth2CredentialId}`;

export const INITIAL_STATE = OAuth2Credentials();
export const STORE_NAME = "oauth2Credentials";
