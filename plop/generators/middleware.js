const { RE_START_ENTRY, RE_ROUTES_ENTRY } = require("../utils");

module.exports = {
  description: "Middleware",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "middleware name"
    }
  ],
  actions: [
    {
      type: "add",
      path: "app/middleware/{{name}}.ts",
      templateFile: "plop/templates/middleware.ts.hbs"
    },
    {
      type: "modify",
      path: "app/routes.ts",
      pattern: RE_START_ENTRY,
      template: 'import { {{name}} } from "./middleware/{{name}}";\n'
    },
    {
      type: "append",
      path: "app/routes.ts",
      pattern: RE_ROUTES_ENTRY,
      template: '\trouter.use("/", {{name}});'
    }
  ]
};
