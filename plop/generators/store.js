const { RE_START_ENTRY, RE_STATE_ENTRY } = require("../utils");

module.exports = {
  description: "Store",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "store name"
    }
  ],
  actions: [
    {
      type: "add",
      path: "app/stores/{{name}}/actions.ts",
      templateFile: "plop/templates/store/actions.ts.hbs"
    },
    {
      type: "add",
      path: "app/stores/{{name}}/definitions.ts",
      templateFile: "plop/templates/store/definitions.ts.hbs"
    },
    {
      type: "add",
      path: "app/stores/{{name}}/index.ts",
      templateFile: "plop/templates/store/index.ts.hbs"
    },
    {
      type: "add",
      path: "app/stores/{{name}}/selectors.ts",
      templateFile: "plop/templates/store/selectors.ts.hbs"
    },
    {
      type: "modify",
      path: "app/stores/index.ts",
      pattern: RE_START_ENTRY,
      template: 'import * as {{name}} from "./{{name}}";\n'
    },
    {
      type: "append",
      path: "app/stores/index.ts",
      template: "export { {{name}} };"
    },
    {
      type: "modify",
      path: "app/lib/state.ts",
      pattern: RE_START_ENTRY,
      template:
        'import { INITIAL_STATE as {{name}} } from "../stores/{{name}}/definitions";\n'
    },
    {
      type: "append",
      path: "app/lib/state.ts",
      pattern: RE_STATE_ENTRY,
      template: "\t{{name}},"
    }
  ]
};
