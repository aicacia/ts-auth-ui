const path = require("path");

module.exports = {
  description: "React Component",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "component name"
    },
    {
      type: "input",
      name: "page",
      default: "",
      message: "page component if component is not in shared components"
    },
    {
      type: "confirm",
      name: "state",
      default: true,
      message: "does the component need state"
    }
  ],
  actions: data => {
    const page = data.page.trim(),
      name = data.name,
      scope = page ? `pages/${page}` : "shared";

    const actions = [];

    if (data.state) {
      actions.push({
        type: "add",
        path: `app/components/${scope}/{{name}}/{{name}}.tsx`,
        data: {
          backPath: path.relative(`./app/components/${scope}/${name}`, `./app`)
        },
        templateFile: "plop/templates/Component/StateComponent.tsx.hbs"
      });
    } else {
      actions.push({
        type: "add",
        path: `app/components/${scope}/{{name}}/{{name}}.tsx`,
        templateFile: "plop/templates/Component/Component.tsx.hbs"
      });
    }

    actions.push({
      type: "add",
      path: `app/components/${scope}/{{name}}/index.ts`,
      templateFile: "plop/templates/Component/export_Component.ts.hbs"
    });

    return actions;
  }
};
